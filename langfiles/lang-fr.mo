��    V      �     |      x     y     �     �  %   �     �  !   �     �  
   �               *  
   6     A  	   J     T     i     ~     �     �     �  0   �  )   �     		     	     	  	   +	     5	     P	     p	     w	     |	     �	     �	     �	  >   �	  +   �	      
     %
     3
     B
  2   I
     |
     �
  
   �
     �
  &   �
     �
     �
     �
       
   &     1     9     M     _     f  
   n     y     �     �  %   �     �     �  	   �     �     �                1     N     e  �   y        *        1     ?     K     S     Z     ^  -   q     �  �   �  5   %     [  !  q     �     �     �     �     �  '   �            %   )     O     ^     n  
   z     �     �     �  	   �     �     �     �  L   �  6   L     �     �     �     �  &   �  '   �               "     +     2     K  K   ^  3   �     �     �            ?        ^     u     y     �  7   �  #   �  '   �  -        A     R     `     h     �     �     �     �     �     �     �  3   �     (     E  	   Q     [  
   d     o      {     �     �     �  �   �     �  /   �     �     �     �     �     �        '        =  �   A  @   �                  7   S   8                      P          I   M       T   
           %      1   A      &          U   2   D   +      9       ;   L   4   /       6   J       !      	   5              =      <   N   R   K      Q   0          *   O                 "   :   H       .          B   )                     $   >             ?   G      ,                                 '              F                 3           -   E                  @   V             (   C          #    (20 Characters) (64 Characters) Acces denied Acces rights for all feed in the OPML Access Access denied to rss subscription Add Add a feed Available in portlet Big section mode Cache delay Categories Category Check all Compact section mode Default subscription Delete Delete Category Description Display mode Do not use proxy for the hosts (comma separated) ERROR: Url is not valid or not accessible Edit Edit Category Edit OPML file Edit feed Encode text (disable html) External news feeds (RSS, Atom) Groups Hour Hour(s) Hours Information feed from %s Information feeds Last published contents on feeds proposed by the administrator Last published contents on subscribed feeds List List of feeds List rss files Modify Modify the information feeds and set access rights My information feeds Name New window No No section, but a link in user section No subscribed feeds No user link, no section Number of items to display OPML subscriptions Opml files Options Proxy configuration RSS access rights Record Rss add Rss modify Rss options Save Scheduled update Section name or entry in user section Set access rights Short title Subscribe Subscribers Subscription Subscriptions The OPML file has been processed The category allready exists The category is in use The name is missing This information feed has not been updated since %s. Probably because of an interruption of service, the feed update has been disabled Title Truncate item description (number of char) Try to update Uncheck all Unknown Update Url Url of source feed View the accessible information feeds content Yes Your RSS link has been turned off automaticly because Ovidentia can't reach distant file, you can re-activate it with this option Your selection exceed the maximum subscription number maximum subscriptions Project-Id-Version: rssfeed
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-08 15:18+0100
PO-Revision-Date: 2014-01-08 15:19+0100
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../
X-Poedit-KeywordsList: rss_translate
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: programs
 (20 Caractères) (64 Caractères) Accès refusé Droits d'accès pour les flux Accès Accès refusé à l'inscription du flux Ajouter Ajouter un flux Rendre disponible en tant que portlet Grande section Délai du cache Catégories Catégorie Sélectionner tout Section compacte Abonnement par défaut Supprimer Supprimer la catégorie Description Mode d'affichage Ne pas utiliser de proxy pour les sites suivants (séparé par des virgules) ERREUR: l'Url n'est pas valide ou n'est pas accessible Modifier Modifier la catégorie Modifier un fichier OPML Modifier Encoder le texte (désactiver le HTML) Flux d'information externes (RSS, Atom) Groupes Heure Heure(s) Heures Flux d'information de %s Flux d'information Les derniers contenus publiés dans les flux proposés par l'administrateur Les derniers contenus publiés dans mes abonnements Liste Liste des flux d'information Liste des liens RSS Modifier Modifier les flux d'information et définir les droits d'accès Mes flux d'information Nom Nouvelle fenêtre Non Pas de section mais un lien dans la section utilisateur Il n'y a pas d'abonnements aux flux Pas de lien utilisateur, pas de section Nombre d'éléments issue du flux à afficher Abonnements OPML Fichiers OPML Options Configuration du serveur proxy Droits d'accès au flux Enregistrer Ajouter Modification du flux Options Enregistrer Mise à jour planifiée Nom de la section ou lien de la section utilisateur Définir les droits d'accès Titre court S'abonner Abonnés Abonnement Abonnements Le fichier OPML à été traité La catégorie existe déjà La catégorie est utilisée Le nom est manquant Ce flux d'information n'a pas été mis à jour depuis le %s. Probablement à cause d'une interruption de service, la mise à jour du flux à été désactivée Titre Tronquer la description (nombre de caractères) Mettre à jour Tout dé-sélectionner Inconnu Mettre à jour Url Source du flux (url) Voir les flux d'information accessibles Oui Votre lien RSS à été désactivé automatiquement car Ovidentia n'a pas pu atteindre le fichier distant, vous pouvez le réactiver avec cette option Votre sélection excède le nombre maximal d'abonnement possible Nombre d'abonnements maximal 