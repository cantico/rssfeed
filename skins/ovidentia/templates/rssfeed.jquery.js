window.rssfeed = new Object();

window.rssfeed.init = function() {
	
	
	jQuery('.rssfeed-portlet').not('.widget-init-done').each(function(index, portlet) {
	    var thisPortlet = this;
		var portlet = jQuery(this).addClass('widget-init-done');
		var meta = window.babAddonWidgets.getMetadata(portlet.attr('id'));
		
		if (typeof meta.mustRevalidate == 'undefined')
		{
			return;
		}
		
		if (meta.mustRevalidate)
		{
			portlet.addClass('loading');
			
			
			// fetch the updated content by ajax
			
			var url = '?addon=rssfeed.main&idx=content&id_feed='+meta.id_feed+'&maxpost='+meta.maxpost;
			
			if (meta.maxlength) {
				url += '&maxlength='+meta.maxlength;
			}
			if (meta.mode) {
				url += '&mode='+meta.mode;
			}
			
			jQuery.get(
				url,
				function(data) {
					
					if ('' == data)
					{
						return;
					}
					
					parent = portlet.parent();
					parent.empty();
					parent.append(data);
					window.bab.init(portlet);
				}
			);
		}
		
	});
	
}


window.bab.addInitFunction(window.rssfeed.init);