<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once( dirname(__FILE__)."/rss_functions.php");
include_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';




function rss_subscription() {

	global $babBody, $babDB;

	if (!$GLOBALS['BAB_SESS_LOGGED']) {
		$babBody->addError(rss_translate('Access denied to rss subscription'));
		return;
	}

	include_once dirname(__FILE__).'/subscription.class.php';


	$tpl = new rss_subscription_tpl($GLOBALS['BAB_SESS_USERID']);

	if (!empty($_POST)) {
		if (rss_subscription_record($GLOBALS['BAB_SESS_USERID'], bab_pp('subscription'))) {
			header('location:'.bab_url::request('tg', 'idx'));
			exit;
		}
	}

	$babBody->babEcho($tpl->getHtml());
}






function rss_list() {
	global $babBody;


	class temp {

		private $collection;
		private $posts;

		public $altbg = true;
		public $subscriptions = false;

		public function __construct() {
			include_once dirname(__FILE__).'/feed.class.php';
			$this->collection = rss_Collection::getFeedIterator($GLOBALS['BAB_SESS_USERID']);
			$id_feed = (int) bab_rp('id_feed');

			$this->t_title = rss_translate('Title');
			$this->t_description = rss_translate('Description');
			$this->t_subscriptions = rss_translate('Subscriptions');
			$this->t_record = rss_translate('Record');

			$this->leftcol = (int) bab_rp('leftcol', 1);

			$sub = rss_Collection::getSubsriptionsIterator();
			if ($sub->count()) {
				$this->subscriptions = $GLOBALS['BAB_SESS_LOGGED'];
				$this->suburl = bab_url::request('tg');
				$this->subscriptions_classname = 'subscriptions';

				if (0 === $id_feed) {
					$this->subscriptions_classname .= ' selected';
				}
			}

			// get iterator for right content
			if (0 === $id_feed) {
				$this->posts = rss_Collection::getMergedSubscriptionsPosts();
				if (rss_Collection::countUserSubscriptions()) {
					$this->description = rss_translate('Last published contents on subscribed feeds');
				} elseif ($sub->count()) {
					$this->description = rss_translate('Last published contents on feeds proposed by the administrator');
				} else {
					$this->description = rss_translate('No subscribed feeds');
				}
			} else {
				if ($this->posts = rss_Collection::getFeedById($id_feed)) {

					$this->description = rss_feedContentToDbEncoding($this->posts->description());
					$this->invalidmessage = false;


					if ('1' !== $this->posts->isUrlValid->__toString()) {

						$lastUpdate = $this->posts->lastUpdate->__toString();

						$this->invalidmessage = sprintf(
							rss_translate('This information feed has not been updated since %s. Probably because of an interruption of service, the feed update has been disabled'),
							bab_shortDate((int) $lastUpdate)
						);

						$this->updateurl = bab_url::mod(bab_url::request('tg', 'id_feed'),'idx', 'unlock');
						$this->t_update = rss_translate('Try to update');
					}

					if ('1' === $this->posts->mustEncodeDescription->__toString()) {
						$this->description = bab_toHtml($this->description);
					}

				} else {
					$this->description = '';
				}
			}


		}

		public function getnextfeed() {

			if ($this->collection->valid()) {

				$this->altbg = !$this->altbg;

				$this->classname = '';

				$id_feed = $this->collection->key();

				$url = bab_url::request('tg');
				$this->url = bab_toHtml(bab_url::mod($url, 'id_feed', $id_feed));

				$this->title = bab_toHtml($this->collection->getTitle());

				$category = $this->collection->getCategoryHeader();
				$this->category = bab_toHtml($category);
				if ($this->collection->isSubscribed()) {
					$this->classname = 'subscribed ';
				}

				if ($id_feed === (int) bab_rp('id_feed')) {
					$this->classname .= 'selected';
				}

				//if (null !== $category) {
				//	$this->altbg = true;
				//}
				$this->collection->next();
				return true;
			}
			return false;
		}





		public function getnextpost() {

			if (!$this->posts) {
				return false;
			}

			if ($this->posts->valid()) {
				$listentry = $this->posts->current();
				$entry = rss_feedEntry::create($listentry);


				$this->link 		= bab_toHtml($entry->link());

				if ('1' === $listentry->mustEncodeDescription->__toString()) {

					$this->title 		= bab_toHtml($entry->title());
					$this->description 	= bab_toHtml(strip_tags($entry->description()), BAB_HTML_ENTITIES | BAB_HTML_P | BAB_HTML_BR);
				} else {

					$this->title 		= rss_sanitize($entry->title());
					$this->description 	= rss_sanitize($entry->description());
				}

				$pubDate = strtotime($entry->pubDate());
				if ($pubDate) {
					$this->pubDate 	= bab_toHtml(bab_longDate($pubDate));
				} else {
					$this->pubDate 	= '';
				}

				$this->posts->next();
				return true;
			}
			return false;
		}



	}


	$temp = new temp();

	$babBody->addStyleSheet('addons/rssfeed/main.css');
	$html = bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath'].'user.html', 'home');

	if (1 === (int) bab_rp('popup')) {
		$babBody->babPopup($html);
	} else {
		$babBody->babEcho($html);
	}
}











function rss_unlock() {

	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

	$id_feed = (int) bab_rp('id_feed');

	global $babDB;

	$babDB->db_query("
		UPDATE ".RSS_FEEDS." SET valid='Y' WHERE id=".$babDB->quote($id_feed)
	);

	//get back to previous page if possible

	if (isset($_SERVER['HTTP_REFERER']))
	{
		$backurl = new bab_url($_SERVER['HTTP_REFERER']);
	} else {
		$backurl = bab_url::get_request('tg', 'id_feed');
	}

	$backurl->location();
}




/**
 * Get feed content after refresh if necessary
 * @param int $id_feed
 *
 */
function rss_getFeedContent($id_feed)
{
	global $babDB;

	if(!bab_isAccessValid('rss_groups',$id_feed)){
		return '';
	}

	$res = $babDB->db_query("SELECT a.* from rss_feeds a WHERE a.portlet = 1 AND id=".$babDB->quote($id_feed)." ORDER BY shorttitle ASC");
	$arr = $babDB->db_fetch_array($res);


	$maxpost = bab_rp('maxpost', 100);
	$maxlength = bab_rp('maxlength', null);

	if (empty($maxlength))
	{
		$maxlength = null;
	}

	$mode = bab_rp('mode', 'list');
	switch ($mode) {
	    case 'slideshow':
	        require_once dirname(__FILE__).'/feed_slideshow.class.php';
	        $listPost = new rss_FeedSlideshowLayout($arr['id'], $arr['title'], $maxpost, $maxlength, false);
	        break;
	    case 'list':
	    default:
	        require_once dirname(__FILE__).'/feed_vbox.class.php';
	        $listPost = new rss_FeedVBoxLayout($arr['id'], $arr['title'], $maxpost, $maxlength, false);
	        break;
	}

	$W = bab_Widgets();

	echo bab_convertStringFromDatabase($listPost->display($W->HtmlCanvas()), 'UTF-8');

	die();
}









// main

if (!$idx = bab_rp('idx')) {
	$idx = "list";
}

if (isset($babBody))
{
	$babBody->addItemMenu("list"			, rss_translate("My information feeds")	, $GLOBALS['babAddonUrl']."main&idx=list");

	if ($GLOBALS['BAB_SESS_LOGGED']) {
		$babBody->addItemMenu("subscription"	, rss_translate("Subscription")			, $GLOBALS['babAddonUrl']."main&idx=subscription");
	}
}

switch ($idx)
	{

	case 'unlock':
		rss_unlock();
		break;

	case 'subscription':
		rss_subscription();
		break;

	case 'content': // ajax
		rss_getFeedContent(bab_rp('id_feed'));
		break;


	default:
	case "list":
		rss_list();
		break;
	}


$babBody->setCurrentItemMenu($idx);