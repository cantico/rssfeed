CREATE TABLE `rss_feeds` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `id_category` int(11) unsigned NOT NULL default '0',
  `title` varchar(64) NOT NULL default '',
  `shorttitle` varchar(20) NOT NULL default '',
  `url` varchar(255) NOT NULL default '',
  `cachetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `cachedata` longblob NOT NULL,
  `valid` enum('Y','N') NOT NULL default 'Y',
  `delay` int(11) unsigned NOT NULL default '3600',
  `encode_description` tinyint(1) unsigned NOT NULL default '0',
  `portlet` tinyint(1) NOT NULL DEFAULT '0',
  `id_opml` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
);


CREATE TABLE `rss_groups` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `id_object` int(11) unsigned NOT NULL default '0',
  `id_group` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `id_object` (`id_object`),
  KEY `id_group` (`id_group`)
);

CREATE TABLE `rss_categories` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
);



CREATE TABLE `rss_subscription` (
  `id_feed` int(11) unsigned NOT NULL default '0',
  `id_user` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id_feed`, `id_user`)
);


CREATE TABLE `rss_opmls` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `url` varchar(255) NOT NULL default '',
  `scheduled_update` tinyint(1) unsigned NOT NULL default '0',
  `lastupdate` datetime NOT NULL default '0000-00-00 00:00:00',
  `rights` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
);