<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
global $babInstallPath;
include_once $babInstallPath.'admin/acl.php';
require_once dirname(__FILE__).'/rss_functions.php';

bab_Functionality::includefile('PortletBackend');

/**
 *
 */
class Func_PortletBackend_rssfeed extends Func_PortletBackend
{
	public function getDescription()
	{
		return rss_translate("External news feeds (RSS, Atom)");
	}

	public function select($category = null)
	{

	    $addon = bab_getAddonInfosInstance('rssfeed');
	    if (!$addon->isAccessValid()) {
	        return array();
	    }
	    
		global $babDB;	
		$query = "SELECT a.* from rss_feeds a LEFT JOIN rss_categories c ON c.id=a.id_category 
			WHERE a.portlet = 1 ";

		if (isset($category))
		{
			$query .= " AND c.name LIKE '".$babDB->db_escape_like($category)."'";
		}

		$query .= "ORDER BY title, shorttitle ASC";
		$res = $babDB->db_query($query);
		$listApp = array();
		while($arr = $babDB->db_fetch_array($res)){
			if(bab_isAccessValid('rss_groups',$arr['id'])){

				$title = $arr['shorttitle'];

				if (empty($title))
				{
					$title = $arr['title'];
				}

				$listApp['rss_'.$arr['id']] = $this->Portlet_rssfeed($arr['id'], $title, $arr['title']);
			}
		}

		return $listApp;
	}



	/**
	 * Get portlet definition instance
	 * @param	string	$portletId			portlet definition ID
	 *
	 * @return portlet_PortletDefinitionInterface
	 */
	public function getPortletDefinition($portletId)
	{
	    $addon = bab_getAddonInfosInstance('rssfeed');
	    if (!$addon || !$addon->isAccessValid()) {
	        return null;
	    }
	    
		$portletId = str_replace('rss_', '', $portletId);

		global $babDB;
		$res = $babDB->db_query("SELECT a.* from rss_feeds a WHERE a.portlet = 1 AND id=".$babDB->quote($portletId)." ORDER BY shorttitle ASC");
		$arr = $babDB->db_fetch_array($res);
		if(bab_isAccessValid('rss_groups',$arr['id'])){

			$url = parse_url($arr['url']);
			$title = sprintf(rss_translate('Information feed from %s'), $url['host']);


			$description = $arr['title'];

			if (empty($description))
			{
				$description = $arr['shorttitle'];
			}


			return $this->Portlet_rssfeed($arr['id'], $title, $description);
		}

		return null;
	}


	public function Portlet_rssfeed($id, $name, $description)
	{
		return new PortletDefinition_rssfeed($id, $name, $description);
	}

	/**
	 * return a list of action to use for configuration
	 * when fired, a parameter named backurl will be added to action, this parameter will contain an url to get back to the main configuration page
	 * each action can contain an icon and a title
	 *
	 * @return multitype:Widget_Action
	 */
	public function getConfigurationActions()
	{

		if (!bab_isUserAdministrator()) {
			return array();
		}

		$addon = bab_getAddonInfosInstance('rssfeed');

		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */

		return array(
			$W->Action()->fromUrl($addon->getUrl().'admin&idx=list')->setTitle(rss_translate('List of feeds')),
			$W->Action()->fromUrl($addon->getUrl().'admin&idx=add')->setTitle(rss_translate('Add a feed'))->setIcon(Func_Icons::ACTIONS_LIST_ADD)
		);
	}

	/**
	 * get a list of categories supported by the backend
	 * @return Array
	 */
	public function getCategories()
	{
		$addon = bab_getAddonInfosInstance('rssfeed');
		if (!$addon->isInstalled())
		{
			return array();
		}

		global $babDB;
		$rescat = $babDB->db_query('SELECT id, name FROM '.RSS_CATEGORIES.' ORDER BY name');
		$categories = array();
		while ($arr = $babDB->db_fetch_assoc($rescat))
		{
			$categories['rssfeed_'.$arr['id']] = $arr['name'];
		}

		return $categories;
	}
}


/////////////////////////////////////////


class PortletDefinition_rssfeed implements portlet_PortletDefinitionInterface
{

	private $id;
	private $name;
	private $description;

	public function __construct($id, $name, $description)
	{
		$this->id = 'rss_'.$id;
		$this->name = $name;
		$this->description = $description;
		$this->addon = bab_getAddonInfosInstance('rssfeed');
	}

	public function getId()
	{
		return $this->id;
	}


	public function getName()
	{
		return $this->name;
	}


	public function getDescription()
	{
		return $this->description;
	}


	public function getPortlet()
	{
		return new Portlet_rssfeed($this->id, $this->name, $this->description);
	}

	/**
	 * Returns the widget rich icon URL.
	 * 128x128 ?
	 *
	 * @return string
	 */
	public function getRichIcon()
	{
		return $this->addon->getStylePath() . 'images/icon48.png';
	}


	/**
	 * Returns the widget icon URL.
	 * 16x16 ?
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return $this->addon->getStylePath() . 'images/icon48.png';
	}

	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return '';
	}

	public function getConfigurationActions()
	{
		if (!bab_isUserAdministrator()) {
			return array();
		}

		list(, $id_feed) = explode('_', $this->getId());

		$addon = bab_getAddonInfosInstance('rssfeed');

		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */

		return array(
			$W->Action()->fromUrl($addon->getUrl().'admin&idx=modif&id='.$id_feed)->setTitle(rss_translate('Edit feed'))->setIcon(Func_Icons::ACTIONS_DOCUMENT_EDIT),
			$W->Action()->fromUrl($addon->getUrl().'admin&idx=groups&id='.$id_feed)->setTitle(rss_translate('Set access rights'))->setIcon(Func_Icons::ACTIONS_SET_ACCESS_RIGHTS)
		);
	}

	public function getPreferenceFields()
	{
        require_once dirname(__FILE__).'/rss_functions.php';

        $modesList = array(
            array('value' => 'list', 'label' => rss_translate('List')),
            array('value' => 'slideshow', 'label' => rss_translate('Slideshow'))
        );

        return $options = array(
            array(
                'label' => rss_translate('Number of items to display'),
                'type' => 'text',
                'name' => 'number'
            ),
            array(
                'label' => rss_translate('Truncate item description (number of char)'),
                'type' => 'text',
                'name' => 'truncate'
            ),
            array(
                'label' => rss_translate('Display mode'),
                'type' => 'list',
                'name' => 'mode',
                'options' => $modesList
            )
        );
	}
}




class Portlet_rssfeed extends Widget_Item implements portlet_PortletInterface
{
	private $id;
	private $name;
	private $description;
	private $options = array();

	/**
	 * Instanciates the widget factory.
	 *
	 * @return Func_Widgets
	 */
	function Widgets()
	{
		return bab_Functionality::get('Widgets');
	}




	/**
	 */
	public function __construct($id, $name, $description)
	{
		$this->id = $id;
		$this->name = $name;
		$this->description = $description;


	}

	/**
	 * @param Widget_Canvas	$canvas
	 * @ignore
	 */
	public function display(Widget_Canvas $canvas)
	{
		$W = $this->Widgets();

		$portletId = str_replace('rss_', '', $this->id);
		global $babDB;
		$res = $babDB->db_query("SELECT a.* from rss_feeds a WHERE a.portlet = 1 AND id=".$babDB->quote($portletId)." ORDER BY shorttitle ASC");
		$arr = $babDB->db_fetch_array($res);

		if(!bab_isAccessValid('rss_groups',$arr['id'])){
			return '';
		}

		$maxpost = empty($this->options['number']) ? 100 : $this->options['number'];
		$truncate = empty($this->options['truncate']) ? null : $this->options['truncate'];

		$mode = isset($this->options['mode']) ? $this->options['mode'] : 'list';
		switch ($mode) {
		    case 'slideshow':
		        require_once dirname(__FILE__).'/feed_slideshow.class.php';
		        $listPost = new rss_FeedSlideshowLayout($arr['id'], $arr['title'], $maxpost, $truncate, true);
		        break;
		    case 'list':
		    default:
		        require_once dirname(__FILE__).'/feed_vbox.class.php';
		        $listPost = new rss_FeedVBoxLayout($arr['id'], $arr['title'], $maxpost, $truncate, true);
		        break;
		}

		$listPost->setMetadata('mode', $mode);

		return $listPost->display($canvas);
	}

	public function getName()
	{
		return get_class($this);
	}

	public function getPortletDefinition()
	{
		return new PortletDefinition_rssfeed($this->id, $this->name, $this->description);
	}

	/**
	 * receive current user configuration from portlet API
	 */
	public function setPreferences(array $configuration)
	{
		$this->options = $configuration;
	}

	public function setPreference($name, $value)
	{
		$this->options[$name] = $value;
	}

	public function setPortletId($id)
	{

	}
}

