<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


function rssfeed_onGroupDeleted($event) {
	
	require_once $GLOBALS['babInstallPath'].'admin/acl.php';
	aclDeleteGroup('rss_groups', $event->id_group);
}



function rssfeed_onUserDeleted($event) {
	
	global $babDB;
	
	$babDB->db_query('DELETE FROM rss_subscription WHERE id_user='.$babDB->quote($event->id_user));
}



function rssfeed_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event) {
	
	require_once dirname(__FILE__).'/rss_functions.php';
	
	$delegations = bab_getUserSitemapDelegations();
	$addon = bab_getAddonInfosInstance('rssfeed');
	
	bab_functionality::includeOriginal('Icons');
	
	
	foreach( $delegations as $key => $deleg ) {
	
		$dg_prefix 	= false === $deleg['id'] ? 'bab' : 'babDG'.$deleg['id'];
		$dg_prefix2 = false === $deleg['id'] ? 'rssfeed' : 'rssfeedDG'.$deleg['id'];
		
		if (bab_isUserAdministrator())
		{
			$position = array('root', $key, $dg_prefix.'Admin', $dg_prefix.'AdminSection');
		
			$item = $event->createItem($dg_prefix2.'Admin');
		
			$item->setLabel(rss_translate("Information feeds"));
			$item->setDescription(rss_translate('Modify the information feeds and set access rights'));
			$item->setLink($addon->getUrl().'admin');
			$item->setPosition($position);
			$item->addIconClassname(Func_Icons::APPS_RSS);
		
			$event->addFunction($item);
		}
	
		if (rss_display_mode() == 1)
		{
			$position = array('root', $key, $dg_prefix.'User', $dg_prefix.'UserSection');
		
			$item = $event->createItem($dg_prefix2.'User');
		
			$item->setLabel(rss_getTitle());
			$item->setDescription(rss_translate('View the accessible information feeds content'));
			$item->setLink($addon->getUrl().'main');
			$item->setPosition($position);
			$item->addIconClassname(Func_Icons::APPS_RSS);
		
			$event->addFunction($item);
		}
	}
}





/**
 * Process scheduled OPML files
 * @param LibTimer_eventDaily $event
 */
function rssfeed_onDaily(LibTimer_eventDaily $event)
{
	require_once dirname(__FILE__).'/opml.class.php';
	global $babDB;
	
	$res = $babDB->db_query('SELECT * FROM rss_opmls WHERE scheduled_update='.$babDB->quote(1));
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$opml = new rss_opml($arr['id'], $arr['url'], $arr['rights']);
		
		$opml->update();
	}
}