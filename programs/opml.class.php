<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

class rss_opml
{
	private $id_opml;
	private $url;
	private $rights;
	
	public function __construct($id_opml, $url, $rights)
	{
		$this->id_opml = $id_opml;
		$this->url = $url;
		$this->rights = $rights;
	}
	
	/**
	 * Get xml content using proxy (if necessary)
	 * @return SimpleXMLElement
	 */
	protected function getXml()
	{
		require_once dirname(__FILE__).'/rss_functions.php';
		$xmlstr = rss_getUrl($this->url);
		
		return new SimpleXMLElement($xmlstr);
	}
	
	/**
	 * @return int
	 */
	protected function getFeedByUrl($url)
	{
		global $babDB;
		
		$res = $babDB->db_query('SELECT * FROM rss_feeds WHERE url='.$babDB->quote($url));
		
		if (0 === $babDB->db_num_rows($res))
		{
			return null;
		}
		
		$arr = $babDB->db_fetch_assoc($res);
		
		return new rss_opmlFeed($this->id_opml, $arr);
	}
	
	/**
	 * Add and delete feeds associated to opml
	 */
	protected function processBody()
	{
		global $babDB;
		require_once $GLOBALS['babInstallPath'].'admin/acl.php';
		
		$xml = $this->getXml();
		$current_ids = array();
		
		foreach($xml->body->outline as $outline)
		{
			$xmlUrl = (string) $outline['xmlUrl'];
			$title = (string) $outline['title'];
			
			
			$feed = $this->getFeedByUrl($xmlUrl);
			
			if (null === $feed)
			{
				$feed = new rss_opmlFeed($this->id_opml, array(
					'url' => $xmlUrl,
					'title' => $title,
					'delay' => (3 * 3600),
					'portlet' => 1
				));
			}
			
			$feed->save();
			
			
			aclSetRightsString('rss_groups', $feed->getId(), $this->rights);
			$current_ids[] = $feed->getId();
		}
		
		$babDB->db_query('DELETE FROM rss_feeds WHERE id NOT IN('.$babDB->quote($current_ids).') AND id_opml='.$babDB->quote($this->id_opml));
	}

	
	public function update()
	{
		$this->processBody();
		
		global $babDB;
		
		$babDB->db_query('UPDATE rss_opmls SET lastupdate=NOW() WHERE id='.$babDB->quote($this->id_opml));
	}
}


class rss_opmlFeed 
{
	private $id_opml;
	private $feed;
	
	public function __construct($id_opml, Array $feed)
	{
		$this->id_opml = $id_opml;
		$this->feed = $feed;
	}
	
	public function save()
	{
		global $babDB;
		
		if (isset($this->feed['id']) && $this->feed['id'] > 0)
		{
			
			if ($this->feed['id_opml'] !== $this->id_opml)
			{
				$babDB->db_query('UPDATE rss_feeds SET id_opml='.$babDB->quote($this->id_opml).' WHERE id='.$babDB->quote($this->feed['id']));
			}
			
		} else {
			
			// insert new feed
			
			$babDB->db_query('INSERT INTO rss_feeds (url, title, delay, portlet, id_opml) 
					VALUES (
						'.$babDB->quote($this->feed['url']).',
						'.$babDB->quote($this->feed['title']).',
						'.$babDB->quote($this->feed['delay']).',
						'.$babDB->quote($this->feed['portlet']).',
						'.$babDB->quote($this->id_opml).'
					 )');
			
			$this->feed['id'] = $babDB->db_insert_id();
		}
	}
	
	
	public function getId()
	{
		return (int) $this->feed['id'];
	}
}