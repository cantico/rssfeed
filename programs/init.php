<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

include_once dirname(__FILE__).'/rss_functions.php';



function rssfeed_onSectionCreate(&$title, &$content)
{
	if (in_array(rss_display_mode(),array(1,4))) {
		return false;
	}

	global $babBody;
	static $nbSections=0;

	include_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	include_once dirname(__FILE__).'/feed.class.php';

	
	$list = new bab_myAddonSection();
	$id = $list->addElement('list');

	if (rss_getOption_Value('mode') == 3)
	{
		// big section
		$feed = rss_Collection::getMergedSubscriptionsPosts();
		foreach($feed as $item) {
			$post = rss_feedEntry::create($item);
			$list->pushHtmlData($id, $post->title(), array('href' => $post->link()));
		}

	} else {
		// small section
		$collection = rss_Collection::getSubsriptionsIterator();
		foreach ($collection as $title) {
			$list->pushHtmlData(
				$id,
				$title, 
				array('href' => $GLOBALS['babAddonUrl']."main&id_feed=".$collection->key())
			);
		}
	}


	$content 	= $list->getHtml();
	$title 		= rss_getTitle();
	$nbSections++;
	return true;
	
}


function rssfeed_upgrade($version_base,$version_ini)
{
	global $babBody, $babDB;

	include_once $GLOBALS['babInstallPath'].'admin/acl.php';
	include_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
	include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
	include_once $GLOBALS['babInstallPath'].'utilit/addonsincl.php';

	// attach events
	bab_addEventListener('bab_eventGroupDeleted'			, 'rssfeed_onGroupDeleted'			, 'addons/rssfeed/events.php'	, 'rssfeed');
	bab_addEventListener('bab_eventUserDeleted'				, 'rssfeed_onUserDeleted'			, 'addons/rssfeed/events.php'	, 'rssfeed');
	bab_addEventListener('bab_eventBeforeSiteMapCreated'	, 'rssfeed_onBeforeSiteMapCreated'	, 'addons/rssfeed/events.php'	, 'rssfeed');
	bab_addEventListener('LibTimer_eventDaily'				, 'rssfeed_onDaily'					, 'addons/rssfeed/events.php'	, 'rssfeed');
	
	$tables = new bab_synchronizeSql(dirname(__FILE__).'/dump.sql');
	
	// bab_installWindow::message(bab_toHtml(print_r($tables->getDifferences(), true), BAB_HTML_ALL));
	
	if ($tables->isCreatedTable('rss_feeds') || empty($version_base))
	{
		// first install, set access rights to all users
		if ($id_object = bab_addonsInfos::getAddonIdByName('rssfeed', false))
		{
			aclSetGroups_all('bab_addons_groups', $id_object);
		}
	}
	
	if ($tables->isEmpty('rss_feeds') && $tables->isEmpty('rss_groups')) {
		if (bab_execSqlFile(dirname(__FILE__).'/flux.sql', bab_Charset::UTF_8)) {
			bab_installWindow::message(bab_toHtml('Rss initialization... done'));
		}
	}

	if (bab_isTable('rss_options')) {
		// put options in registry

		$options = $babDB->db_fetch_assoc($babDB->db_query('SELECT * FROM rss_options'));

		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/rssfeed/options/');

		$registry->setKeyValue('mode'					, (int) $options['mode']);
		$registry->setKeyValue('addon_title'			, $options['section_name']); // title for section or entry in sitemap

		$babDB->db_query('DROP TABLE rss_options');
	
	}

	@bab_functionality::includefile('PortletBackend');
	if (class_exists('Func_PortletBackend')) {
		$addonName = 'rssfeed';
		$addonInfo = bab_getAddonInfosInstance($addonName);
		
		$addonPhpPath = $addonInfo->getPhpPath();
		require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
		$functionalities = new bab_functionalities();
		$functionalities->registerClass('Func_PortletBackend_rssfeed', $addonPhpPath . 'portletbackend.class.php');
	}

	
	return true;
}

function rssfeed_onDeleteAddon()
{
	
	// detach events
	include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
	bab_removeEventListener('bab_eventGroupDeleted'			, 'rssfeed_onGroupDeleted'			, 'addons/rssfeed/events.php');
	bab_removeEventListener('bab_eventUserDeleted'			, 'rssfeed_onUserDeleted'			, 'addons/rssfeed/events.php');
	bab_removeEventListener('bab_eventBeforeSiteMapCreated'	, 'rssfeed_onBeforeSiteMapCreated'	, 'addons/rssfeed/events.php');
	bab_removeEventListener('LibTimer_eventDaily'			, 'rssfeed_onDaily'					, 'addons/rssfeed/events.php');
	
	
	@bab_functionality::includefile('PortletBackend');
	if (class_exists('Func_PortletBackend')) {
		$addonName = 'rssfeed';
		$addonInfo = bab_getAddonInfosInstance($addonName);
	
		$addonPhpPath = $addonInfo->getPhpPath();
		require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
		$functionalities = new bab_functionalities();
		$functionalities->unregister('PortletBackend/rssfeed');
	}
	
	return true;
}
