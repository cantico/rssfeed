<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
include_once dirname(__FILE__).'/feed.class.php';








class rss_subscription_tpl {

	private $collection;
	public $altbg = true;

	/**
	 * @param	int		$id_user			0 for default subscription, or id_user
	 * @param	bool	$access_control		
	 */
	public function __construct($id_user, $access_control = true) {
		$this->collection = rss_Collection::getFeedIterator($id_user, $access_control);

		$this->t_title = rss_translate('Title');
		$this->t_description = rss_translate('Description');
		$this->t_subscribe = rss_translate('Subscribe');
		$this->t_record = rss_translate('Record');
	}

	public function getnext() {

		if ($this->collection->valid()) {

			$this->altbg = !$this->altbg;

			$this->id_feed = $this->collection->key();
			$this->title = bab_toHtml($this->collection->getTitle());
			$this->description = bab_toHtml($this->collection->getFeedDescription());

			$category = $this->collection->getCategoryHeader();
			$this->category = bab_toHtml($category);
			$this->checked = $this->collection->isSubscribed();

			if (null !== $category) {
				$this->altbg = true;
			}

			$this->collection->next();
			return true;
		}
		return false;
	}

	public function getHtml() {

		$addon = bab_getAddonInfosInstance('rssfeed');

		return bab_printTemplate($this, $addon->getRelativePath().'main.html', 'subscription');
	}
}



function rss_subscription_record($id_user, $arr) {
	global $babDB, $babBody;

	$max = (int) rss_getOption_Value('max_subscriptions');
	if ($max < count($arr)) {
		$babBody->addError(rss_translate('Your selection exceed the maximum subscription number'));
		return false;
	}

	$babDB->db_query('DELETE FROM '.RSS_SUBSCRIPTION.' WHERE id_user='.$babDB->quote($id_user));

	if ($arr) {
		foreach($arr as $id_feed => $dummy) {
			$babDB->db_query('INSERT INTO '.RSS_SUBSCRIPTION.' 
				(id_feed, id_user) 
			VALUES 
				('.$babDB->quote($id_feed).', '.$babDB->quote($id_user).')
			');
		}
	}

	return true;
}