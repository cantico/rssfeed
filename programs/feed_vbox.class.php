<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


bab_Widgets()->includePhpClass('Widget_VBoxLayout');


class rss_FeedVBoxLayout extends Widget_VBoxLayout
{
	
	/**
	 * 
	 * @param int 		$id_feed		
	 * @param string 	$title			Feed title
	 * @param int		$maxpost
	 * @param int		$maxlength		Truncate post description, number of characters
	 * @param bool		$cache_only		Get only from cache
	 * @param unknown_type $id
	 */
	public function __construct($id_feed, $title, $maxpost = 100, $maxlength = null, $cache_only = false, $id = null)
	{
		parent::__construct($id);
		
		$this->setVerticalSpacing(.5,'em');
		$this->addClass('rssfeed-portlet');
		$W = bab_Widgets();
		
		$this->setMetadata('id_feed', $id_feed);
		$this->setMetadata('maxpost', $maxpost);
		$this->setMetadata('maxlength', $maxlength);
		
		$this->addItem($W->Title($title, 2)->addClass('title'));
		
		
		include_once dirname(__FILE__).'/feed.class.php';
		
		if ($posts = rss_Collection::getFeedById($id_feed, $cache_only)) {

			if ($posts->mustRevalidate->__toString() && $cache_only)
			{
				$this->setMetadata('mustRevalidate', true);
			}
			
			if ($invalid = $this->getInvalidFrame($id_feed, $posts))
			{
				$this->addItem($invalid);
			}
			
			$i = 0;
			foreach($posts as $post){
				if($i < $maxpost){
					if ($posts->valid()) {
						$this->addItem($this->getPostLayout($post, $maxlength));
						$i++;
					}
				}
			}
		} else {
			$this->setMetadata('mustRevalidate', true);
		}
		
	}
	
	
	
	protected function getInvalidFrame($id_feed, Zend_Feed_Abstract $posts)
	{
		if ('1' === $posts->isUrlValid->__toString()) {
			return null;	
		}
		
		$W = bab_Widgets();
		
		$lastUpdate = $posts->lastUpdate->__toString();
	
		$invalidmessage = sprintf(
				rss_translate('This information feed has not been updated since %s. Probably because of an interruption of service, the feed update has been disabled'),
				bab_shortDate((int) $lastUpdate)
		);
	
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	
		$updateurl = new bab_url();
		$updateurl->tg = 'addon/rssfeed/main';
		$updateurl->idx = 'unlock';
		$updateurl->id_feed = $id_feed;
		$t_update = rss_translate('Try to update');
	
		return $W->Frame()->addClass('notify')->addItem($W->Link($t_update, $updateurl->toString()))->addItem($W->Label($invalidmessage));
	}
	
	
	
	protected function getPostLayout($listentry, $maxlength = null)
	{
		$W = bab_Widgets();
		$postLayout = $W->VBoxLayout()->addClass('entry');
		
		$entry = rss_feedEntry::create($listentry);
		
		
		
		if ('1' === $listentry->mustEncodeDescription->__toString()) {
			$title 		= bab_toHtml($entry->title());
			$description 	= bab_toHtml(strip_tags($entry->description()), BAB_HTML_ENTITIES | BAB_HTML_P | BAB_HTML_BR);
		} else {
		
			$title 		= rss_sanitize($entry->title());
			$description 	= rss_sanitize($entry->description());
		}
		
		$pubDate = strtotime($entry->pubDate());
		if ($pubDate) {
			$_Date 	= bab_shortDate($pubDate, false);
			$_Hour	= bab_time($pubDate);
		} else {
			$_Date 	= '';
			$_Hour	= '';
		}
		
		$items = $W->Items();
		
		if ($_Date && $_Hour)
		{
			$items->addItem($W->VBoxItems($W->Label($_Date)->addClass('date'), $W->Label($_Hour)->addClass('hour'))->addClass('pubdate'));
		}
		
		$items->addItem(
				$W->Title($W->Link($title, $entry->link())->addAttribute('target', '_blank'),3)->addClass('title')
		);
		
		$postLayout->addItem($items);

		if(isset($maxlength)){
			$text = bab_unhtmlentities(strip_tags($description));
			$text = preg_replace('/(\r\n|\n|\r)+\s*/', "\n", $text);
			$description = bab_toHtml(bab_abbr($text, BAB_ABBR_FULL_WORDS, $maxlength), BAB_HTML_BR);
		}
		
		
		$postLayout->addItem(
				$W->Html($description)
		);
		
		return $postLayout;
	}
	
	
	public function display(Widget_Canvas $canvas)
	{
		return '
		<style type="text/css" scoped>
		
		.rssfeed-portlet .entry {
			padding:.4em;
			border-bottom:#eee 1px solid;
		}
		
		.rssfeed-portlet .widget-layout-vbox-item:last-child .entry {
			border-bottom:none;
		}
		
		.rssfeed-portlet h3.title {
			margin:0 0 .5em 0 !important;
			background:none !important;
			padding:0 !important;
		}
		
		.rssfeed-portlet .title a {
			text-decoration:none;
		}
		
		.rssfeed-portlet .pubdate {
			white-space: nowrap;
			float:right;
			
			border: 1px solid rgba(0,0,0,0.1);
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			background-color: #eee;
			background-color: rgba(100,100,100,0.05);
			
			margin:0 0 .2em .2em;
			padding:.2em .8em;
			
			font-size:9px;
			font-family:Arial;
			text-align:center;
			
			color:#444;
		}
		
		.rssfeed-portlet .pubdate .hour {
			font-size:120%;
			font-weight:bold;
		}
		
		.rssfeed-portlet .notify {
			
			padding:.7em .5em .7em 25px;
			background:#ffffbf url(\''.$GLOBALS['babInstallPath'].'skins/ovidentia/images/addons/rssfeed/exclamation.png\') no-repeat 4px 50%;
			border:#cfcf3d 1px solid;
			color:#5e5e35;
		}
		
		
		
		.rssfeed-portlet .notify a {
			border:#444 1px solid;
			background:#777;
			color:#fff;
			text-decoration:none;
			padding:.05em .5em;
			margin:0 .5em;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			
			float:right;
		}
		
		
		.rssfeed-portlet .notify a:hover {
			border:#444 1px solid;
			background:#000;
			color:#fff;
		}
		
		</style>
		'
		.parent::display($canvas)
		.$canvas->metadata($this->getId(), $this->getMetadata())
		.$canvas->loadScript($this->getId(), bab_getAddonInfosInstance('rssfeed')->getTemplatePath().'rssfeed.jquery.js');
	}
}