<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once( dirname(__FILE__)."/rss_functions.php");
require_once( dirname(__FILE__)."/feed.class.php");

function rssfeed_ovml($args)
{
	if (isset($args['action']))
		{
		

		switch($args['action'])
			{
			case 'rsslist':

				$subscriptions = 0;
				if (isset($args['subscriptions'])) {
					if (is_numeric($args['subscriptions'])) {
						 $subscriptions = (int) $args['subscriptions'];
					}
				}


				if (0 === $subscriptions) {
					$collection = rss_Collection::getFeedIterator();
				} else {
					$collection = rss_Collection::getSubsriptionsIterator();
				}

				$feed = array();
				$return = array();

				foreach ($collection as $FeedId => $FeedTitle)
					{
					$feed['FeedTitle'] = $collection->getTitle();
					$feed['FeedShortTitle'] = $collection->getShortTitle();
					$feed['FeedDescription'] = $collection->getFeedDescription();
					$feed['FeedUrl'] = $collection->getUrl();
					$feed['FeedId'] = $FeedId;
					$feed['FeedCount'] = $collection->countEntries();
					
					$return[] = $feed;
					}

				

				return $return;


			case 'itemlist':
				
				//L'id du flux est obligatoire
				if (!isset($args['feedid']) && !is_numeric($args['feedid'])) {
					return false;
				}

				$feed = rss_Collection::getFeedById($args['feedid']);
				
				if (!$feed) {
					return array();
				}

				$return = array();
				
				//les param�tres index et rows peuvent �tre activ�s
				
				$from = 0;
				if (isset($args['index'])) {
					if (is_numeric($args['index'])) {
						 $from = (int) $args['index'];
					}
				}

				$to = $feed->count();
				if (isset($args['rows'])) {
					if (is_numeric($args['rows'])) {
						 $to = $from + $args['rows'];
					}
				}

				foreach($feed as $key => $item) {
					if ($key >= $from && $key < $to) {

						$feedEntry = rss_feedEntry::create($item);

						$element = array();
						$element['ItemTitle'] 		= $feedEntry->title();
						$element['ItemLink'] 		= $feedEntry->link();
						$element['ItemDescription'] = $feedEntry->description();
						$element['ItemPubDate'] 	= strtotime($feedEntry->pubDate());
						$return[] = $element;
					}
				}

				return $return;
			}

		}

}

?>
