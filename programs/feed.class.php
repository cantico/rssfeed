<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
include_once dirname(__FILE__).'/rss_functions.php';

/**
 * collection of feed for user
 */
class rss_Collection implements seekableIterator {

	/**
	 * MySql ressource
	 */
	private $res;

	/**
	 * current feed inforations stored in table
	 * array
	 */
	private $feed = null;


	private function __construct() {

	}
	
	/**
	 * Get cached feed data
	 * @return string|false
	 */
	private function getCache() {
		if (empty($this->feed['cachedata'])) {
			return false;
		}
		
		return $this->feed['cachedata'];
	}
	
	
	/**
	 * @return bool
	 */
	private function mustRevalidate() {
		$age = time() - bab_mktime($this->feed['cachetime']);
		return ($age > $this->feed['delay']);
	}


	/**
	 * Get feed data, cache feed data, return cached data
	 * @return 	string | false			RSS content or false if invalid feed
	 */
	private function updateCache() {

		global $babDB;
		

		if (false === $this->isUrlValid() || !$this->mustRevalidate()) {
			
			// if URL fetching disabled (invalid URL), return last cache
			// if cache not expired, return last cache
			
			return $this->getCache();
		}

		$fcontents = rss_getUrl($this->feed['url']);
	
		if (rss_isFeedContentValid($fcontents)) {

			// feed is valid, cache data and return data

			$babDB->db_query("
				UPDATE ".RSS_FEEDS." SET 
					cachetime=NOW(), 
					cachedata=".$babDB->quote($fcontents)."  
				WHERE 
					id=".$babDB->quote($this->feed['id'])
			);

			return $fcontents;

		} else {
			
			// feed is invalid, disable fetching
			$babDB->db_query("
				UPDATE ".RSS_FEEDS." SET valid='N' WHERE id=".$babDB->quote($this->feed['id'])
			);

			return false;
		}	
	}

	/**
	 * Get array of id feed for the current user
	 * default subscription or personnal subscription without control of access rights
	 * @param	int		$id_user
	 * @return array
	 */
	private static function getUserSubscription($id_user = null) {
		global $babDB;
		$max = (int) rss_getOption_Value('max_subscriptions');

		if (null === $id_user) {
			$id_user = $GLOBALS['BAB_SESS_USERID'];
		}

		$req = 'SELECT id_feed FROM '.RSS_SUBSCRIPTION.' WHERE id_user = '.$babDB->quote($id_user).' ';

		if (0 !== $max) {
			$req .= ' LIMIT 0,'.$max;
		}

		$return = array();
		$res = $babDB->db_query($req);
		while ($arr = $babDB->db_fetch_assoc($res)) {
			$id_feed = (int) $arr['id_feed'];
			$return[$id_feed] = $id_feed;
		}

		if (0 !== $id_user && empty($return)) {
			return self::getUserSubscription(0);
		}

		return $return;
	}



	/**
	 * Return seekableIterator for all feeds
	 * @param	int		$subscription	the id_user used for subscription informations (0 = default subscription)
	 * @param	bool	$access_control
	 * @return	rss_Collection
	 */
	public static function getFeedIterator($subscription = 0, $access_control = true) {
		$list = new rss_Collection;

		if (true === $access_control) {
			$list->queryFromMultipleId($subscription, bab_getUserIdObjects(RSS_GROUPS));
		} else {
			$list->queryFromMultipleId($subscription);
		}

		return $list;
	}



	/**
	 * Return seekableIterator for all subscribed feeds for current user
	 * @return	rss_Collection
	 */
	public static function getSubsriptionsIterator() {
		$list = new rss_Collection;
		$arr = array_intersect(self::getUserSubscription(), bab_getUserIdObjects(RSS_GROUPS));
		$list->queryFromMultipleId($GLOBALS['BAB_SESS_USERID'], $arr);
		return $list;
	}


	/**
	 * Get feed by ID
	 * @param	int		$id_feed
	 * @param	bool	$cache_only		if cache_only=true use the additional property "mustRevalidate" to update displayed data
	 * 
	 * @return Zend_Feed_Abstract | false
	 */
	public static function getFeedById($id_feed, $cache_only = false) {

		if (!bab_isAccessValid(RSS_GROUPS, $id_feed)) {
			return false;
		}

		$list = new rss_Collection;

		$list->queryFromMultipleId(0, array($id_feed => $id_feed));
		$list->next();

		if ($cache_only)
		{
			$feed = $list->getFeedCache();
		} else {
			$feed = $list->getFeed();
		}

		if (!$feed) {
			return false;
		}

		
		$feed->mustRevalidate = $list->mustRevalidate();
		$feed->mustEncodeDescription = $list->mustEncodeDescription();
		$feed->isUrlValid = $list->isUrlValid();
		$feed->lastUpdate = $list->lastUpdate();

		foreach($feed as $post) {
			if ($post instanceOf Zend_Feed_Entry_Abstract) {
				$post->mustEncodeDescription = $list->mustEncodeDescription();
			}
		}	

		$feed->rewind();

		return $feed;
	}



	/**
	 * merge all posts from subscriptions feeds in one array
	 * posts are ordered by publication date
	 * @return ArrayIterator
	 */
	public static function getMergedSubscriptionsPosts() {

		$posts = array();
		$collection = self::getSubsriptionsIterator();

		foreach($collection as $title) {

			$zendfeed = $collection->getFeed();
			if ($zendfeed) {
				foreach($zendfeed as $key => $entry) {

					$entry->mustEncodeDescription = $collection->mustEncodeDescription();
					$pubDate = $entry->pubDate();

					if ($pubDate) {
						
						$pubDate = date('YmdHis',strtotime($pubDate));

						
						$pubDate .= sprintf('%010d', $collection->key()).sprintf('%010d', $key);
						

						$posts[$pubDate] = $entry;

					} else {
						$posts[] = $entry;
					}
				}
			}
		}

		krsort($posts);
		
		$iterator = new ArrayIterator(array_values($posts));

		return $iterator;
	}




	/**
	 * Count user subscriptions, without the default subscriptions set by admin
	 * @return int
	 */
	public static function countUserSubscriptions() {
		global $babDB;
		
		$res = $babDB->db_query('
			SELECT 
				COUNT(*) subscriptions 

			FROM '.RSS_FEEDS.' f, '.RSS_SUBSCRIPTION.' s 
			WHERE 
				f.id=s.id_feed 
				AND s.id_user='.$babDB->quote($GLOBALS['BAB_SESS_USERID']).'
		');

		$arr = $babDB->db_fetch_assoc($res);

		return (int) $arr['subscriptions'];
	}



	/**
	 * Query mysql for the list of feeds
	 * @param	int		$subscription	the id_user used for subscription informations (0 = default subscription)
	 * @param	array	$arr			the list of ID, if null all ID will be loaded
	 * @return bool
	 */
	private function queryFromMultipleId($subscription = 0, $arr = null) {
		global $babDB;
		
		$req = 'SELECT 
				f.*, 
				s.id_feed subscription,
				COUNT(stats.id_user) subscription_stats, 
				c.name category 

			FROM '.RSS_FEEDS.' f 
				LEFT JOIN '.RSS_SUBSCRIPTION.' s ON s.id_feed=f.id AND s.id_user='.$babDB->quote($subscription).' 
				LEFT JOIN '.RSS_SUBSCRIPTION.' stats ON stats.id_feed=f.id AND stats.id_user<>\'0\' 
				LEFT JOIN '.RSS_CATEGORIES.' c ON c.id=f.id_category
		';

		if (null !== $arr) {
			$req .= ' WHERE f.id IN('.$babDB->quote($arr).')
			';
		}

		$req .= ' GROUP BY f.id ORDER BY category, title';
		$this->res = $babDB->db_query($req);
	}


	public function current() {

		if (false === $this->feed) {
			return false;
		}

		return $this->feed['title'];
	}

	public function key() {

		return (int) $this->feed['id'];
	}
	
	public function next() {
		global $babDB;

		$this->feed = $babDB->db_fetch_assoc($this->res);
	}

	public function rewind() {
		$this->seek(0);
	}

	public function valid() {

		if (null === $this->feed) {
			$this->next();
		}

		return $this->feed !== false;
	}

	public function seek($pos) {
		global $babDB;
		if ($this->count()) {
			$babDB->db_data_seek($this->res, $pos);
		}
	}


	/**
	 * Count feeds in current collection
	 * @return int
	 */
	public function count() {
		
		global $babDB;
		return $babDB->db_num_rows($this->res);
	}


	public function getSubscriberNumber() {

		return (int) $this->feed['subscription_stats'];
	}

	/**
	 * @return	Zend_Feed_Abstract | false
	 */
	public function getFeedCache()
	{
		$feedcontent = $this->getCache();
		
		if (!$feedcontent) {
			return false;
		}
		
		
		rss_includeFeed();
		
		try {
			$obj = Zend_Feed::importString($feedcontent);
		
		} catch(Zend_Feed_Exception $e) {
			// invalid feed
			bab_debug($e->getMessage());
			return false;
		}
		
		return $obj;
	}


	/**
	 * @return	Zend_Feed_Abstract | false
	 */
	public function getFeed() {
		
		$feedcontent = $this->updateCache();

		if (!$feedcontent) {
			return false;
		}


		rss_includeFeed();

		try {
			$obj = Zend_Feed::importString($feedcontent);

		} catch(Zend_Feed_Exception $e) {
			// invalid feed
			bab_debug($e->getMessage());
			return false;
		}

		return $obj;
	}

	/**
	 * Test if feed has been disabled because of fetching error or not
	 * @return boolean
	 */
	public function isUrlValid() {
		return 'Y' === $this->feed['valid'];
	}


	/**
	 * Last time the feed has been updated
	 * @return int	UNIX Timestamp
	 */
	public function lastUpdate() {
		return bab_mktime($this->feed['cachetime']);
	}


	/**
	 * Test if current user is subscribed to current feed
	 * @return bool
	 */
	public function isSubscribed() {

		return !is_null($this->feed['subscription']);
	}

	/**
	 * test if the description must be encoded
	 * @return bool
	 */
	public function mustEncodeDescription() {
		return (bool) $this->feed['encode_description'];
	}




	/**
	 * URL of the feed
	 * @return string
	 */
	public function getUrl() {
		return $this->feed['url'];
	}

	/**
	 * count entries in current field
	 */
	public function countEntries() {

		$obj = $this->getFeed();

		if (false === $obj) {
			return 0;
		}


		return $obj->count();
	}

	/**
	 * Title stored in database
	 * @return string
	 */
	public function getTitle() {
		

		$title = trim($this->feed['title']);

		if (empty($title)) {
			$title = rss_translate('Unknown');
		}
		return $title;
	}


	/**
	 * Title stored in database
	 * @return string
	 */
	public function getShortTitle() {
		$shorttitle = $this->feed['shorttitle'];

		if (empty($shorttitle)) {
			$shorttitle = bab_abbr($this->getTitle(), BAB_ABBR_INITIAL, 18);
		}

		return $shorttitle;
	}

	
	/**
	 * Category stored in database
	 * @return string
	 */
	public function getCategory() {
		return (string) $this->feed['category'];
	}


	/**
	 * Category stored in database or null if the last call of this method returned the same result
	 * used to display headers while the feeds are crawled with the iterator
	 * @return string | null
	 */
	public function getCategoryHeader() {
		static $last = null;
		$current = $this->getCategory();
	
		if (null === $last || $current !== $last) {
			$last = $current;
			return $current;
		}

		return null;
	}


	/**
	 * Get cache delay in hours
	 * @return int
	 */
	public function getDelay() {
		$delay = (int) $this->feed['delay'];
		return $delay/3600;
	}


	/**
	 * Title stored inside the feed
	 * @return string
	 */
	public function getFeedTitle() {
		$obj = $this->getFeed();

		if (false === $obj) {
			return '';
		}

		$title = (string) $obj->title();

		return rss_feedContentToDbEncoding($title);
	}

	/**
	 * Link stored inside the feed
	 * @return string
	 */
	public function getFeedLink() {
		$obj = $this->getFeed();

		if (false === $obj) {
			return '';
		}

		$link = $obj->link();

		if (!is_string($link)) {
			if (null === $link) {
				return '';
			}

			if (0 < count($link)) {
				foreach($link as $temp) {
					if ('link' === $temp->nodeName) {
						$link = $temp->textContent;
						break;
					}
				}
			}
		}


		return rss_feedContentToDbEncoding($link);
	}

	/**
	 * Description stored inside the feed
	 * @return string
	 */
	public function getFeedDescription() {
		$obj = $this->getFeed();

		if (false === $obj) {
			return '';
		}

		return rss_feedContentToDbEncoding($obj->description());
	}


}








class rss_feedEntry {

	/**
	 * RSS or ATOM data
	 */
	private $zendEntry;

	private function __construct() {

	}


	/**
	 * create a feed entry from a zend feed entry RSS or ATOM
	 * @param	Zend_Feed_Entry_Abstract	$zendEntry
	 * @return	rss_feedEntry
	 */
	public static function create(Zend_Feed_Entry_Abstract $zendEntry) {

		$obj = new rss_feedEntry;
		$obj->zendEntry = $zendEntry;
		return $obj;
	}

	/**
	 * Get common Entry information beetween RSS and ATOM
	 * Name reference is RSS
	 * <ul>	
	 *	<li>title (titre) : The title of the item</li>
	 *	<li>link (lien) : The URL of the item</li>
	 *	<li>description : A synopsis of the item</li>
	 *  <li>author (auteur) : The author's email address</li>
	 *	<li>pubDate (date de publication) : The date the item was published, in RFC 822 / RFC 2822 date format</li>
	 * </ul>
	 * 
	 * @param	string	$methodname
	 * @param	mixed						$unused
	 * @return 	string
	 */
	public function __call($methodname, $unused) {

		$data = null;

		if ('Zend_Feed_Entry_Rss' === get_class($this->zendEntry)) {
			
			$data = $this->zendEntry->$methodname();
			
			switch($methodname) {

				case 'pubDate':
					if (null === $data) {
						// Use dublin core metadata if available <dc:date>
						$data = $this->zendEntry->date();
					}
					break;
			}
		}

		if ('Zend_Feed_Entry_Atom' === get_class($this->zendEntry)) {

			// translate method names for ATOM standard

			switch($methodname) {

				case 'title':
					$data = $this->zendEntry->title();
					break;

				case 'link':
					foreach((array) $this->zendEntry->link as $link) {
					    $data = $this->zendEntry->link('alternate');
						break;
						}
					break;
					
				case 'description':		    
					if ($this->zendEntry->subtitle()) {
						$data = $this->zendEntry->subtitle();
						break;
					}	
					else if ($this->zendEntry->summary()) {
						$data = $this->zendEntry->summary();
						break;
					}	
					$data = $this->zendEntry->content();
					break;
					
				case 'author':
					if ($this->zendEntry->author && $this->zendEntry->author->email()) {
						$data = $this->zendEntry->author->email();
					}
					break;
					
				case 'pubDate':
					if ($this->zendEntry->published()) {
						$data = date('r', strtotime($this->zendEntry->published()));
					}
					else if ($this->zendEntry->updated()) {
						$data = date('r', strtotime($this->zendEntry->updated()));
					}
					break;


				default:
					trigger_error('Unsuported method : '.$methodname);
			}
		}

		// HACK to fix non-string values
		// there are multiple nodes if a node with same name dans different namespace exists
		if (!is_string($data)) {
			$temp = '';
			foreach((array) $data as $node) {
				if ('DOMElement' === get_class($node)) {
					$temp .= $node->textContent;
					break;
				}
			}
			$data = $temp;
		}

		$data = rss_feedContentToDbEncoding($data);

		return $data;
		
	}
}
