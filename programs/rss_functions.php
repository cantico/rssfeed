<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


define ("RSS_FEEDS", "rss_feeds");
define ("RSS_GROUPS", "rss_groups");
define ("RSS_CATEGORIES", "rss_categories");
define ("RSS_SUBSCRIPTION", "rss_subscription");
define ("RSS_REDIR_ON_MODIF", true);


function rss_getOption_Value($opt)
	{
	$registry = bab_getRegistryInstance();
	$registry->changeDirectory('/rssfeed/options/');
	return $registry->getValue($opt);
	}

function rss_getTitle() {
	$title = rss_getOption_Value('addon_title');

	if (empty($title)) {
		$title = rss_translate('My information feeds');
	}
	return $title;
}


/**
 * Display mode
 * 
 * 1 = No section, but a link in user section
 * 2 = Compact section mode
 * 3 = Big section mode
 * 4 = No user link, no section
 * 
 * @return int
 */
function rss_display_mode() {
	$mode = rss_getOption_Value('mode');
	
	if (null === $mode) {
		return 1;
	}
	
	return $mode;
}



function rss_translate($str, $str_plurals = null, $number = null) {
	
	if ($translate = bab_functionality::get('Translate/Gettext'))
	{
		/* @var $translate Func_Translate_Gettext */
		$translate->setAddonName('rssfeed');
	
		return $translate->translate($str, $str_plurals, $number);
	}
	
	return $str;
}

/**
 * Include zend files
 */
function rss_includeZend()
{
	
	// use zend framework if available
	
	$zend = @bab_functionality::get('ZendFramework');
	if (false !== $zend) {
		$zend->setIncludePath();
		return true;
	}
	
	$new_path = dirname(__FILE__);
	$includepath = explode(PATH_SEPARATOR, get_include_path());
	
	if (!in_array($new_path, $includepath)) {
		array_unshift($includepath, $new_path);
	}
	if (false === set_include_path(implode(PATH_SEPARATOR, $includepath))) {
		return false;
	}
	
	return true;
}


/**
 * Include file for feed
 * @see Zend_Feed
 */
function rss_includeFeed() {
	
	if (class_exists('Zend_Feed', false))
	{
		return true;
	}

	if (!rss_includeZend())
	{
		return false;
	}
	
	require_once 'Zend/Feed.php';
}






/**
 * Test feed content Validity
 * @return boolean
 */
function rss_isFeedContentValid($content)
{
	if (empty($content)) {
		bab_debug('feed invalid because empty content');
		return false;
	}

	rss_includeFeed();

	try {
		Zend_Feed::importString($content);
	} catch(Zend_Feed_Exception $e) {
		// invalid feed
		bab_debug($e->getMessage());
		return false;
	}
	
	
	return true;
}



/**
 * Get feed title from content without cache
 * @param	string	$content	Must be valid feed content
 * @return string
 */
function rss_getFeedTitle($content)
{
	rss_includeFeed();
	try {
		$feed = Zend_Feed::importString($content);
	} catch (Exception $e) {
		return '';
	}

	if (!is_string($feed->title())) {
		return '';
	}

	return rss_feedContentToDbEncoding($feed->title());
}



/**
 * Convert a feed content in UTF-8 to database encoding
 * @param string $str
 * @return string
 */
function rss_feedContentToDbEncoding($str)
{
	if ('UTF-8' === bab_Charset::getIso())
	{
		return $str;
	}
	
	return iconv('UTF-8', bab_charset::getIso().'//TRANSLIT//IGNORE', $str);
	//return bab_getStringAccordingToDataBase($str, 'UTF-8');
}


/**
 * Get URL content with proxy parameters
 * @param string $url
 * @param string $proxy_name
 * @param int $proxy_port
 * @return string			feed content or false if fetching error
 */
function rss_getUrlWithoutCurl($url, $proxy_name, $proxy_port)
{
	if ($proxy_name)
	{
		$content = '';
	
		$proxy_fp = @fsockopen($proxy_name, $proxy_port);
		if (!$proxy_fp)
		{
			bab_debug('Error : can\'t connect to proxy : '.$proxy_name.':'.$proxy_port);
			return false;
		}
		fputs($proxy_fp, "GET $url HTTP/1.0\r\nHost: ".$proxy_name."\r\n\r\n");
		while(!feof($proxy_fp))
		{
			$content .= fread($proxy_fp, 4096);
		}
		fclose($proxy_fp);
		$content = substr($content, strpos($content,"\r\n\r\n")+4);
	
	}
	else
	{
		$arr = @file($url);
		if (false === $arr) {
			bab_debug('Error : can\'t fetch url : '.$url);
			return false;
		}
	
		$content = join( '',  $arr);
	}
	
	return $content;
}


/**
 * Get URL content with proxy parameters
 * 
 * some servers return a 403 if the user agent is php
 * http://www.estrepublicain.fr/territoire-de-belfort/rss
 * 
 * @param string $url
 * @param string $proxy_name
 * @param int $proxy_port
 * 
 * @return string			feed content or false if fetching error
 */
function rss_getUrlWithCurl($url, $proxy_name, $proxy_port)
{
	$ch = curl_init();
	
	if ($proxy_name)
	{
		$proxy = $proxy_name.':'.$proxy_port;
		//$proxyauth = 'user:password';
	
		curl_setopt($ch, CURLOPT_PROXY, $proxy);
		//curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	}
	
	
	curl_setopt($ch,CURLOPT_USERAGENT		,'Ovidentia RSS reader');
	curl_setopt($ch, CURLOPT_URL			, $url);
	curl_setopt($ch, CURLOPT_HEADER         , 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 4);
	curl_setopt($ch, CURLOPT_TIMEOUT		, 4);
	
	$content = curl_exec($ch);
	
	if($errno = curl_errno($ch)) {
	    bab_debug('CURL ERROR: '.$errno);
	    if (function_exists('curl_strerror')) {
		    bab_debug(curl_strerror($errno));
	    }
		return rss_getUrlWithoutCurl($url, $proxy_name, $proxy_port);
	}
	$info = curl_getinfo($ch);
	curl_close($ch);
	
	
	if($info['http_code'] != 200)
	{
		bab_debug($info);
		bab_debug('Error : can\'t fetch url : '.$url.' http code '.$info['http_code']);
		return false;
	}
	
	if (false === $content) {
		bab_debug('Error : can\'t fetch url : '.$url);
		return false;
	}
	
	
	
	return $content;
}


/**
 * Get URL content with proxy parameters
 * @param	string	$url	url to feed
 * @return string			feed content or false if fetching error
 */
function rss_getUrl($url)
{
	$proxy_name = rss_getOption_Value('proxy_name');
	$proxy_port = rss_getOption_Value('proxy_port');
	$proxy_exceptions = rss_getOption_Value('proxy_exceptions');
	
	$exceptions = array();
	if ($proxy_exceptions) {
		$exceptions = preg_split('/[\s,]+/', $proxy_exceptions);
	}
	
	$purl = parse_url($url);
	
	if (in_array($purl['host'], $exceptions))
	{
		$proxy_name = '';
	}
	
	if (!function_exists('curl_init'))
	{
		return rss_getUrlWithoutCurl($url, $proxy_name, $proxy_port);
	}
		

	return rss_getUrlWithCurl($url, $proxy_name, $proxy_port);
}

	
	
/**
 * Remove differences beetween ISO-885-1/CP1252 and ISO-8859-15
 * @param string $str
 * @return string
 */	
function rss_sanitize($str) {
	
	if ('UTF-8' === bab_Charset::getIso())
	{
		return $str;
	}
	
	// 					euro		apostrophe 	apostrophe	apostrophe	oe			OE
	//								CP1252		CP1252		ISO-8859-1
	$source 	= array(chr(0x80),	chr(0x91),	chr(0x92),	chr(0xB4),	chr(0x9C),	chr(0x8C),	chr(0x93),	chr(0x94),	chr(0x85),	chr(0x96),	chr(0x97),	chr(0x88),	chr(0x99),		chr(0x8B),	chr(0x9B),	chr(0x84),	chr(0x95),	chr(0x89),	chr(0x83),	chr(0x86));
	$replace	= array(chr(0xA4),	"'",		"'",		"'",		chr(0xBD),	chr(0xBC),	'"',		'"',		'...',		'-',		'-',		'^',		'<sup>TM<sup>',	'&lsaquo;',	'&rsaquo;',	'&bdquo;',	'&bull;',	'&permil;',	'&fnof;',	'&dagger;');
	
	return str_replace($source, $replace	, $str);
	
}

