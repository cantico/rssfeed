;<?php/*

[general]
name							="rssfeed"
version							="5.9.7"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="RSS agreator for Ovidentia"
description.fr					="Module de syndication de contenu pour le format RSS ou Atom"
long_description.fr             ="README.md"
delete							=1
db_prefix						="rss_"
author							="Paul de Rosanbo"
ov_version						="8.0.94"
php_version						="5.1.2"
mysql_version					="4.1.2"
icon							="icon.png"
configuration_page				="admin&idx=options"
mod_dom							="Available"
mod_iconv						="Available"
tags                            ="extension,default,portlet"

[recommendations]
mod_curl						="Available"

[addons]
widgets							="1.0.9"
LibTranslate					=">=1.12.0rc3.01"


;*/?>
