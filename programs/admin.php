<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once(dirname(__FILE__)."/rss_functions.php");


function rsslist()
	{
	global $babBody;
	class temp
		{
		var $collection;
		var $altbg = true;
		function temp()
			{
			global $babBody;
			$this->t_edit = rss_translate("Edit");
			$this->t_title = rss_translate("Title");
			$this->t_url = rss_translate("Url");
			$this->t_delay = rss_translate("Cache delay");
			$this->t_unit = rss_translate("Hour");
			$this->t_groups = rss_translate("Access");
			$this->t_delete = rss_translate("Delete");
			$this->t_add = rss_translate("Add");
			$this->t_category = rss_translate("Category");
			$this->uncheckall = rss_translate("Uncheck all");
			$this->checkall = rss_translate("Check all");
			$this->update = rss_translate("Update");
			$this->t_subscriber_number = rss_translate("Subscribers");

			require_once(dirname(__FILE__)."/feed.class.php");
			
			$this->collection = rss_Collection::getFeedIterator(0, false);
			}


		function getnext()
			{
			if($this->collection->valid())
				{
				$this->altbg = !$this->altbg;
				$id = $this->collection->key();
				
				$this->accessurl = bab_toHtml($GLOBALS['babAddonUrl']."admin&idx=modif&id=".$id);
				$this->groupsurl = bab_toHtml($GLOBALS['babAddonUrl']."admin&idx=groups&id=".$id);
				$this->arr['title'] = bab_toHtml($this->collection->getTitle());
				$this->arr['category'] = bab_toHtml($this->collection->getCategory());
				$this->arr['delay'] = $this->collection->getDelay();
				$this->t_unit = (1 !== (int) $this->arr['delay'] ) ? rss_translate("Hours") : rss_translate("Hour");
				$this->arr['subscription_stats'] = bab_toHtml($this->collection->getSubscriberNumber());
				$this->collection->next();
				return true;
				}
			else
				return false;

			}
		}

	$temp = new temp();
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."main.html", "rsslist"));
	}
	

function rssmodif($id="")
	{
	global $babBody;
	class temp
		{

		function temp($id)
			{
			global $babBody, $babDB;
			$this->t_title = rss_translate("Title");
			$this->t_url = rss_translate("Url of source feed");
			$this->t_delay = rss_translate("Cache delay");
			$this->t_title_length = rss_translate("(64 Characters)");
			$this->t_shorttitle_length = rss_translate("(20 Characters)");
			$this->t_unit = rss_translate("Hour(s)");
			$this->t_shorttitle = rss_translate("Short title");
			$this->t_blank = rss_translate("New window");
			$this->t_yes = rss_translate("Yes");
			$this->t_no = rss_translate("No");
			$this->t_valid = rss_translate("Your RSS link has been turned off automaticly because Ovidentia can't reach distant file, you can re-activate it with this option");
			$this->t_record = rss_translate("Record");
			$this->t_delete = rss_translate("Delete");
			$this->t_category = rss_translate("Category");
			$this->t_encode_description = rss_translate("Encode text (disable html)");

			$this->t_portlet = rss_translate('Available in portlet');

			$this->id=$id;
			$this->backurl = bab_toHtml(bab_rp('backurl'));

			if ($id != "")
				{
				$req = "select * from ".RSS_FEEDS." WHERE id=".$babDB->quote($id);
				$this->res = $babDB->db_query($req);
				$this->arr = $babDB->db_fetch_array($this->res);
				$this->arr['delay'] = $this->arr['delay']/3600;
				$this->del = true;
				}
			else
				{
    			$this->arr['title'] 			= '';
				$this->arr['shorttitle'] 		= '';
				$this->arr['url'] 				= '';
				$this->arr['delay'] 			= '3';
				$this->arr['valid'] 			= 'Y';
				$this->arr['encode_description']= '0';
				$this->arr['portlet'] 			= 1;
				}

			$this->rescat = $babDB->db_query('SELECT * FROM '.RSS_CATEGORIES.' ORDER BY name');
			
			$this->portlets = (false !== @bab_functionality::get('PortletBackend'));

			}


			function getnextcategory() {
				global $babDB;
				if ($arr = $babDB->db_fetch_assoc($this->rescat)) {

					$this->id_category = bab_toHtml($arr['id']);
					$this->name = bab_toHtml($arr['name']);

					$this->selected = (isset($this->arr['id_category']) && $arr['id'] === $this->arr['id_category']);

					return true;
				}

				return false;
			}

		}
	$temp = new temp($id);
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."main.html", "rssmodif"));
	}






function rsscategories() {



	global $babBody;
	class temp
		{
		var $altbg = true;
		function temp()
			{
			global $babBody;
			$this->t_edit = rss_translate("Edit");
			$this->t_name = rss_translate("Name");
			$this->t_in_use = rss_translate("The category is in use");
			$this->t_delete = rss_translate("Delete");
			$this->t_add = rss_translate("Add");
			
			$this->db = $GLOBALS['babDB'];
			$req = '
				SELECT 
					c.id, c.name, COUNT(f.id) feeds 
				FROM '.RSS_CATEGORIES.' c 
					LEFT JOIN '.RSS_FEEDS.' f ON c.id=f.id_category 
				GROUP BY c.id 
				ORDER BY c.name
			';
			$this->res = $this->db->db_query($req);
			$this->count = $this->db->db_num_rows($this->res);
			}


		function getnext()
			{
			static $i = 0;
			if( $i < $this->count)
				{
				$this->altbg = !$this->altbg;
				$arr = $this->db->db_fetch_array($this->res);

				$this->editurl = bab_toHtml($GLOBALS['babAddonUrl'].'admin&idx=edit_category&id_category='.$arr['id']);
				$this->deleteurl = bab_toHtml($GLOBALS['babAddonUrl'].'admin&idx=delete_category&id_category='.$arr['id']);
				$this->name = bab_toHtml($arr['name']);


				$this->deletable = 0 === (int) $arr['feeds'];

				$i++;
				return true;
				}
			else
				return false;

			}
		}

	$temp = new temp();
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."main.html", "categories"));


}




function rsscategory() {

	

	global $babBody;
	class temp
		{
		var $altbg = true;
		function temp()
			{
			global $babBody, $babDB;
			$this->t_name = rss_translate("Name");
			$this->t_record = rss_translate("Record");

			if (bab_rp('id_category')) {
			
				$req = "select * from ".RSS_CATEGORIES.' WHERE id='.$babDB->quote(bab_rp('id_category'));
				$res = $babDB->db_query($req);

				$arr = $babDB->db_fetch_assoc($res);

				$this->name = bab_toHtml($arr['name']);
				$this->id_category = bab_toHtml(bab_rp('id_category'));
				}
			else {
				$this->name = '';
				$this->id_category = '';
				}

			}
		}

	function rsscategory_record() {

		global $babBody,$babDB;

		$id_category = (int) bab_pp('id_category');
		$name = bab_pp('name');

		if (empty($name)) {
			$babBody->addError(rss_translate('The name is missing'));
			return false;
		}




		if ($id_category) {

			$res = $babDB->db_queryWem('UPDATE '.RSS_CATEGORIES.' 
				SET name='.$babDB->quote($name).' WHERE id='.$babDB->quote($id_category));
		} else {

			$res = $babDB->db_queryWem('INSERT INTO '.RSS_CATEGORIES.' (name) VALUES ('.$babDB->quote($name).')');
		}

		if (!$res) {
			$babBody->addError(rss_translate('The category allready exists'));
			return false;
		}

		header("location:".$GLOBALS['babAddonUrl']."admin&idx=categories");
		exit;
	}


	if (!empty($_POST)) {
		rsscategory_record();
	}

	$temp = new temp();
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."main.html", "category"));


}



function rsscategory_delete() {

	$id_category = (int) bab_rp('id_category');

	global $babDB;
	$babDB->db_query('DELETE FROM '.RSS_CATEGORIES.' WHERE id='.$babDB->quote($id_category));

	header("location:".$GLOBALS['babAddonUrl']."admin&idx=categories");
	exit;
}



	
function rssoptions()
	{
	global $babBody;
	class temp
		{

		function temp()
			{
			global $babBody;
			
			$this->t_mode = rss_translate("Display mode");
			$this->t_mode1 = rss_translate("No section, but a link in user section");
			$this->t_mode2 = rss_translate("Compact section mode");
			$this->t_mode3 = rss_translate("Big section mode");
			$this->t_mode4 = rss_translate("No user link, no section");
			$this->t_max_subscriptions = rss_translate("maximum subscriptions");
			$this->t_section_name = rss_translate("Section name or entry in user section");
			$this->t_proxy = rss_translate("Proxy configuration");
			$this->t_proxy_exeptions = rss_translate("Do not use proxy for the hosts (comma separated)");
			$this->t_record = rss_translate("Record");

			$registry = bab_getRegistryInstance();
			$registry->changeDirectory('/rssfeed/options/');

			if (!empty($_POST)) {

				$this->mode 				= bab_toHtml(bab_pp('mode'));
				$this->addon_title			= bab_toHtml(bab_pp('addon_title'));
				$this->max_subscriptions	= bab_toHtml(bab_pp('max_subscriptions'));
				$this->proxy_name			= bab_toHtml(bab_pp('proxy_name'));
				$this->proxy_port			= bab_toHtml(bab_pp('proxy_port'));
				$this->proxy_exceptions		= bab_toHtml(bab_pp('proxy_exceptions'));

			} else {

				$this->mode 				= bab_toHtml($registry->getValue('mode'));
				$this->addon_title			= bab_toHtml($registry->getValue('addon_title'));
				$this->max_subscriptions	= bab_toHtml($registry->getValue('max_subscriptions'));
				$this->proxy_name			= bab_toHtml($registry->getValue('proxy_name'));
				$this->proxy_port			= bab_toHtml($registry->getValue('proxy_port'));
				$this->proxy_exceptions		= bab_toHtml($registry->getValue('proxy_exceptions', ''));
			}

		}

		

	}
	$temp = new temp();
	$babBody->babecho(bab_printTemplate($temp, $GLOBALS['babAddonHtmlPath']."main.html", "rssoptions"));
	}







function rss_default_subscription() {
	include_once dirname(__FILE__).'/subscription.class.php';
	include_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	global $babBody, $babDB;
	$tpl = new rss_subscription_tpl(0, false);

	if (!empty($_POST)) {
		if (rss_subscription_record(0, bab_pp('subscription'))) {
			header('location:'.bab_url::request('tg', 'idx'));
			exit;
		}
	}
	
	$babBody->babEcho($tpl->getHtml());
}


/**
 * get feed and content from url
 * 
 * @param	string	$url	url of feed or web page
 * @return array | false	with two text key : url (url of feed used), content (string of feed content to use)
 */
function rss_getFeedUrlAndContent($url) {

	$content = rss_getUrl($url);

	if (rss_isFeedContentValid($content)) {

		// URL is valid feed

		return array(
			'url' 		=> $url,
			'content' 	=> $content
		);
	}


	rss_includeFeed();

	try {
        $contents = rss_getUrl($url);

		if (!$contents) {
			return false;
		}

		$pattern = '~(<link[^>]+)/?>~i';
		$matches = null;
        $result = @preg_match_all($pattern, $contents, $matches);

		if ($result === false) {
            return false;
        }

		// Try to fetch a feed for each link tag that appears to refer to a feed
        $feeds = array();
        if (isset($matches[1]) && count($matches[1]) > 0) {
            foreach ($matches[1] as $link) {
                // force string to be an utf-8 one
                if (!mb_check_encoding($link, 'UTF-8')) {
                    $link = mb_convert_encoding($link, 'UTF-8');
                }
                $xml = @simplexml_load_string(rtrim($link, ' /') . ' />');
                if ($xml === false) {
                    continue;
                }
                $attributes = $xml->attributes();
                if (!isset($attributes['rel']) || !@preg_match('~^(?:alternate|service\.feed)~i', $attributes['rel'])) {
                    continue;
                }
                if (!isset($attributes['type']) ||
                        !@preg_match('~^application/(?:atom|rss|rdf)\+xml~', $attributes['type'])) {
                    continue;
                }
                if (!isset($attributes['href'])) {
                    continue;
                }
                try {
                    // checks if we need to canonize the given uri
                    try {
                    	rss_includeZend();
						require_once 'Zend/Http/Client.php';
                        $uri = Zend_Uri::factory((string) $attributes['href']);
                    } catch (Zend_Uri_Exception $e) {
                        // canonize the uri
                        $path = (string) $attributes['href'];
                        $query = $fragment = '';
                        if (substr($path, 0, 1) != '/') {
                            // add the current root path to this one
							$currentpath = Zend_Uri::factory($url)->getPath();
                            $path = substr($currentpath, 0, -1 * strlen(strrchr($currentpath, '/'))) . '/' . $path;
                        }
                        if (strpos($path, '?') !== false) {
                            list($path, $query) = explode('?', $path, 2);
                        }
                        if (strpos($query, '#') !== false) {
                            list($query, $fragment) = explode('#', $query, 2);
                        }
                        $uri = Zend_Uri::factory($url);
                        $uri->setPath($path);
                        $uri->setQuery($query);
                        $uri->setFragment($fragment);
                    }

                } catch (Exception $e) {
                    continue;
                }
                $feeds[] = $uri;
            }
        }



		if (is_array($feeds) && 0 < count($feeds)) {
			$url = reset($feeds);
			$content = rss_getUrl($url);

			return array(
				'url' 		=> $url,
				'content' 	=> $content
			);
		}
		
		
	} catch(Exception $e) {

	}

	return false;
}





function rss_feed_record() {

	global $babBody, $babDB;

	$arr = rss_getFeedUrlAndContent(bab_pp('url'));

	if (false === $arr) {
		$babBody->msgerror = rss_translate("ERROR: Url is not valid or not accessible");
		return false;
	}

	$url 	 = $arr['url'];
	$content = $arr['content'];

	$title = bab_pp('title');
	if (empty($title)) {
		$title = rss_getFeedTitle($content);
	}

	$shorttitle = bab_pp('shorttitle');


	if (is_numeric($_POST['id']))
		{
		
		$valid = (isset($_POST['valid']) && $_POST['valid'] == 'Y') ? ",valid='Y'" : "";
		$babDB->db_query("
			UPDATE ".RSS_FEEDS." SET 
				title=" .$babDB->quote($title). ",
				shorttitle=".$babDB->quote($shorttitle).",
				url=" . $babDB->quote($url). ",
				delay=".$babDB->quote(($_POST['delay']*3600)).",
				id_category=".$babDB->quote($_POST['id_category']).", 
				encode_description=".$babDB->quote(bab_pp('encode_description', 0)).",
				portlet=".$babDB->quote(bab_pp('portlet', 0))."
				 ".$valid." 
				

			WHERE id=".$babDB->quote($_POST['id'])."
			");
		
	}
	else
		{

		$babDB->db_query("INSERT INTO ".RSS_FEEDS." 
			(title,shorttitle, url, delay, id_category, encode_description, portlet) 
		values 
			(
			" . $babDB->quote($title) . ", 
			" . $babDB->quote($shorttitle) . ",
			" . $babDB->quote($url)	. ",
			" . $babDB->quote($_POST['delay']*3600). ",
			" . $babDB->quote($_POST['id_category']). ",
			" . $babDB->quote(bab_pp('encode_description', 0)). ",
			" . $babDB->quote(bab_pp('portlet', 0)). "
			)");
		
		$id_feed = $babDB->db_insert_id();
		
		require_once $GLOBALS['babInstallPath'].'admin/acl.php';
		aclAdd('rss_groups', BAB_ADMINISTRATOR_GROUP, $id_feed);
	}
	
	// set access to administrators by default
	
	
	
	
	if ($backurl = bab_pp('backurl'))
	{
		header('location:'.$backurl);
		exit;
	}

	return true;
}


/**
 * Acl form
 */
function rss_adm_access() {

	global $babInstallPath, $babBody;
	include_once $babInstallPath."admin/acl.php";

	if (bab_rp('setaccess')) {
		maclGroups();
		if ($backurl = bab_pp('backurl'))
		{
			header('location:'.$backurl);
			exit;
		} else {
			header('location:?tg=addon/rssfeed/admin&idx=list');
			exit;
		}
	}

	$macl = new macl(bab_rp('tg'), bab_rp('idx'), bab_rp('id'), 'setaccess');
	$macl->addtable( RSS_GROUPS ,$babBody->title);
	
	if ($backurl = bab_rp('backurl'))
	{
		$macl->set_hidden_field('backurl', $backurl);
	}
	
	$macl->babecho();
}



function rss_opmls()
{
	require_once $GLOBALS['babInstallPath'] . 'utilit/toolbar.class.php';
	global $babDB;
	$W = bab_Widgets();
	bab_functionality::includeOriginal('Icons');
	$addon = bab_getAddonInfosInstance('rssfeed');
	
	$page = $W->BabPage();
	$table = $W->BabTableView();
	$table->addClass(Func_Icons::ICON_LEFT_16);
	
	$page->setTitle(rss_translate('Opml files'));
	
	$toolbar = new BAB_Toolbar();
	$page->addStyleSheet('toolbar.css');
	
	$toolbar->addToolbarItem(
			new BAB_ToolbarItem(rss_translate('Add'), $addon->getUrl().'admin&idx=editopml',
					$GLOBALS['babInstallPath'] . 'skins/ovidentia/images/Puces/addSmall.png', '', '', '')
	);
	
	$page->addItem($W->Html($toolbar->printTemplate()));
	
	$page->addItem($table);
	
	$res = $babDB->db_query('SELECT * FROM rss_opmls ORDER BY url');
	
	$row = 0;
	
	$table->addHeadRow($row);
	$table->addItem($W->Label(rss_translate('Edit')), $row, 0);
	$table->addItem($W->Label(rss_translate('Url')), $row, 1);
	$table->addItem($W->Label(rss_translate('Scheduled update')), $row, 2);
	$table->addItem($W->Label(rss_translate('Update')), $row, 3);
	$row++;
	
	
	
	while($arr = $babDB->db_fetch_assoc($res))
	{
		$table->addItem($W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT), $addon->getUrl().'admin&idx=editopml&id_opml='.$arr['id']), $row, 0);
		$table->addItem($W->Label($arr['url']), $row, 1);
		$table->addItem($W->Label($arr['scheduled_update'] ? rss_translate('Yes') : rss_translate('No')), $row, 2);
		$table->addItem($W->Link($W->Icon('', Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD), $addon->getUrl().'admin&idx=updateopml&id_opml='.$arr['id']), $row, 3);
		$row++;
	}
	
	
	$page->displayHtml();
	
}



function rss_updateopml()
{
	require_once dirname(__FILE__).'/opml.class.php';
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	
	$id_opml = (int) bab_rp('id_opml', 0);
	global $babDB, $babBody;
	
	if (!$id_opml)
	{
		return;
	}
	
	$res = $babDB->db_query('SELECT * FROM rss_opmls WHERE id='.$babDB->quote($id_opml));
	while ($arr = $babDB->db_fetch_assoc($res))
	{
		$opml = new rss_opml($arr['id'], $arr['url'], $arr['rights']);
		$opml->update();
	}
	
	/*@var $babBody babBody */
	
	$babBody->addNextPageMessage(rss_translate('The OPML file has been processed'));
	
	$url = bab_url::get_request('tg');
	$url->idx = 'opmls';
	$url->location();
}



function rss_editopml()
{
	global $babDB;
	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	$id_opml = (int) bab_rp('id_opml', 0);
	
	if (!empty($_POST))
	{
		if ($id_opml > 0)
		{
			$babDB->db_query('UPDATE rss_opmls SET 
					url='.$babDB->quote(bab_pp('url')).', 
					scheduled_update='.$babDB->quote(bab_pp('scheduled_update')).',
					rights='.$babDB->quote(bab_pp('rights')).' 
				WHERE id='.$babDB->quote($id_opml));
		} else {
			$babDB->db_query('INSERT INTO rss_opmls (url, scheduled_update, rights) 
					VALUES ('.$babDB->quote(bab_pp('url')).', '.$babDB->quote(bab_pp('scheduled_update')).', '.$babDB->quote(bab_pp('rights')).')');
		}
		
		
		$url = bab_url::get_request('tg');
		$url->idx = 'opmls';
		$url->location();
	}
	
	$W = bab_Widgets();
	bab_functionality::includeOriginal('Icons');
	$addon = bab_getAddonInfosInstance('rssfeed');
	
	$page = $W->BabPage();
	$page->setTitle(rss_translate('Edit OPML file'));
	
	$editor = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(1,'em'));
	$editor->addClass('widget-bordered');
	$editor->addClass('BabLoginMenuBackground');
	
	
	if ($id_opml > 0)
	{
		$res = $babDB->db_query('SELECT * FROM rss_opmls WHERE id='.$babDB->quote($id_opml));
		$arr = $babDB->db_fetch_assoc($res);
		
		$editor->setValues($arr);
	}
	
	
	$editor->addItem($W->LabelledWidget(rss_translate('Url'), $W->LineEdit()->setSize(50), 'url'));
	$editor->addItem($W->LabelledWidget(rss_translate('Scheduled update'), $W->CheckBox(), 'scheduled_update'));
	$editor->addItem($W->Acl()->setName('rights')->setTitle(rss_translate('Acces rights for all feed in the OPML')));
	
	$editor->addItem($W->SubmitButton()->setLabel(rss_translate('Save')));
	$editor->setSelfPageHiddenFields();
	
	
	$page->addItem($editor);
	
	$page->displayHtml();
}




// main

if (!bab_isUserAdministrator()) {
	$babBody->msgerror = rss_translate("Acces denied");
    return;
}


$idx = bab_rp('idx', "list");


if (isset($_POST['action']))
	switch ($_POST['action'])
		{
		case "modif":
			if (!rss_feed_record()) {
				$idx='modif';
			}
			break;

		case "delete":
			if (bab_pp('id')) {
				$babDB->db_query("DELETE FROM ".RSS_FEEDS." WHERE id=".$babDB->quote(bab_pp('id')));
			}
			break;

		case "options":
			if (isset($_POST['mode']))
				{
				$registry = bab_getRegistryInstance();
				$registry->changeDirectory('/rssfeed/options/');
				$registry->setKeyValue('mode', (int) bab_pp('mode'));
				$registry->setKeyValue('addon_title', bab_pp('addon_title'));
				$registry->setKeyValue('max_subscriptions', (int) bab_pp('max_subscriptions'));
				$registry->setKeyValue('proxy_name', bab_pp('proxy_name'));
				$registry->setKeyValue('proxy_port', (int) bab_pp('proxy_port'));
				$registry->setKeyValue('proxy_exceptions', bab_pp('proxy_exceptions'));
				
				if (RSS_REDIR_ON_MODIF) {
					bab_siteMap::clearAll();
					header("location:".$GLOBALS['babAddonUrl']."admin&idx=options");
					exit;
					}
				}
			break;
		}



$babBody->addItemMenu("list", rss_translate("List"), $GLOBALS['babAddonUrl']."admin&idx=list");
$babBody->addItemMenu("categories", rss_translate("Categories"), $GLOBALS['babAddonUrl']."admin&idx=categories");
$babBody->addItemMenu("opmls", rss_translate("OPML subscriptions"), $GLOBALS['babAddonUrl']."admin&idx=opmls");
$babBody->addItemMenu("options", rss_translate("Options"), $GLOBALS['babAddonUrl']."admin&idx=options");

$babBody->addItemMenu("default_subscription", rss_translate("Default subscription"), $GLOBALS['babAddonUrl']."admin&idx=default_subscription");

switch ($idx)
	{
	case "groups":
		$babBody->title = rss_translate("RSS access rights");
		$babBody->addItemMenu("groups", rss_translate("Groups"), $GLOBALS['babAddonUrl']."admin&idx=groups");
		rss_adm_access();
		
	break;

	case "modif":
		$babBody->title = rss_translate("Rss modify");
		$babBody->addItemMenu("modif", rss_translate("Modify"), $GLOBALS['babAddonUrl']."admin&idx=modif");
		rssmodif(bab_rp('id'));
	break;

	case "add":
		$babBody->title = rss_translate("Rss add");
		rssmodif();
	break;

	case "categories":
		$babBody->title = rss_translate("Categories");
		rsscategories();
	break;

	
	case "edit_category":
		$babBody->title = rss_translate("Edit Category");
		rsscategory();
	break;

	case "delete_category":
		$babBody->title = rss_translate("Delete Category");
		rsscategory_delete();
	break;

	case 'default_subscription':
		$babBody->title = rss_translate("Default subscription");
		rss_default_subscription();
	break;


	case "options":
		$babBody->title = rss_translate("Rss options");
		rssoptions();
	break;
	
	case 'opmls':
		rss_opmls();
		break;
		
	case 'editopml':
		rss_editopml();
		break;
		
	case 'updateopml':
		rss_updateopml();
		break;

	default:
	case "list":
		$babBody->title = rss_translate("List rss files");
		rsslist();
	break;
	}
	
$babBody->setCurrentItemMenu($idx);
