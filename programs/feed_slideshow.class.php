<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


bab_Widgets()->includePhpClass('Widget_VBoxLayout');


class rss_FeedSlideshowLayout extends Widget_VBoxLayout
{

	/**
	 *
	 * @param int 		$id_feed
	 * @param string 	$title			Feed title
	 * @param int		$maxpost
	 * @param int		$maxlength		Truncate post description, number of characters
	 * @param bool		$cache_only		Get only from cache
	 * @param unknown_type $id
	 */
	public function __construct($id_feed, $title, $maxpost = 100, $maxlength = null, $cache_only = false, $id = null)
	{
		parent::__construct($id);

		$this->setVerticalSpacing(.5,'em');
		$this->addClass('rssfeed-portlet');
		$W = bab_Widgets();

		$this->setMetadata('id_feed', $id_feed);
		$this->setMetadata('maxpost', $maxpost);
		$this->setMetadata('maxlength', $maxlength);

		$this->addItem($W->Title($title, 2)->addClass('title'));


		$html = '';

		include_once dirname(__FILE__).'/feed.class.php';

		if ($posts = rss_Collection::getFeedById($id_feed, $cache_only)) {

			if ($posts->mustRevalidate->__toString() && $cache_only)
			{
				$this->setMetadata('mustRevalidate', true);
			}

			if ($invalid = $this->getInvalidFrame($id_feed, $posts))
			{
				$this->addItem($invalid);
			}

			$html .= '<div id="bab-owl-carousel-rss" class="bab-owl-carousel owl-carousel owl-theme">';


			$i = 0;
			foreach($posts as $post){
				if($i < $maxpost){
					if ($posts->valid()) {
						$html .= $this->getPostLayout($post, $maxlength);
						$i++;
					}
				}
			}

			$html .= '</div>';

			$this->addItem($W->Html($html));
		} else {
			$this->setMetadata('mustRevalidate', true);
		}

	}



	protected function getInvalidFrame($id_feed, Zend_Feed_Abstract $posts)
	{
		if ('1' === $posts->isUrlValid->__toString()) {
			return null;
		}

		$W = bab_Widgets();

		$lastUpdate = $posts->lastUpdate->__toString();

		$invalidmessage = sprintf(
				rss_translate('This information feed has not been updated since %s. Probably because of an interruption of service, the feed update has been disabled'),
				bab_shortDate((int) $lastUpdate)
		);

		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

		$updateurl = new bab_url();
		$updateurl->tg = 'addon/rssfeed/main';
		$updateurl->idx = 'unlock';
		$updateurl->id_feed = $id_feed;
		$t_update = rss_translate('Try to update');

		return $W->Frame()->addClass('notify')->addItem($W->Link($t_update, $updateurl->toString()))->addItem($W->Label($invalidmessage));
	}



    protected function getPostLayout($listentry, $maxlength = null)
    {
        $entry = rss_feedEntry::create($listentry);

        if ('1' === $listentry->mustEncodeDescription->__toString()) {
            $title 		= bab_toHtml($entry->title());
            $description 	= bab_toHtml(strip_tags($entry->description()), BAB_HTML_ENTITIES | BAB_HTML_P | BAB_HTML_BR);
        } else {
            $title 		= rss_sanitize($entry->title());
            $description 	= rss_sanitize($entry->description());
        }

        $matches = null;
        if (preg_match('/src="([^"]*)"/', $description, $matches)) {
            $imageUrl = $matches[1];
        } elseif (preg_match('/src=([^\s>]*)/', $description, $matches)) {
            $imageUrl = $matches[1];
        } else {
            return '';
        }

        $pubDate = strtotime($entry->pubDate());
        if ($pubDate) {
            $_Date 	= bab_shortDate($pubDate, false);
            $_Hour	= bab_time($pubDate);
        } else {
            $_Date 	= '';
            $_Hour	= '';
        }

        $html = '';
        $html .= '<div class="item">';
        $html .= '  <div class="relative-owl">';
        $html .= '    <a title="' . bab_toHtml($entry->title()) . '" href="' . bab_toHtml($entry->link()) . '">';
        $html .= '      <img alt="' . bab_toHtml($entry->title()) . '" src="' . bab_toHtml($imageUrl) . '" />';
        $html .= '    </a>';
        $html .= '    <div class="owl-item-article">';
        $html .= '      <div class="article-title">';
        $html .= '        <a href="' . bab_toHtml($entry->link()) . '">' . bab_toHtml($entry->title()) . '</a>';
        $html .= '      </div>';
        $html .= '      <div class="articleintro">' .bab_toHtml(strip_tags($entry->description()), BAB_HTML_ENTITIES | BAB_HTML_P | BAB_HTML_BR) . '</div>';
        $html .= '    </div>';
        $html .= '  </div>';
        $html .= '</div>';

		return $html;
	}


    public function display(Widget_Canvas $canvas)
    {
        return parent::display($canvas)
            .$canvas->metadata($this->getId(), $this->getMetadata())
            .$canvas->loadStyleSheet(bab_getAddonInfosInstance('rssfeed')->getStylePath().'owl.carousel.css')
            .$canvas->loadStyleSheet(bab_getAddonInfosInstance('rssfeed')->getStylePath().'owl.theme.css')
            .$canvas->loadStyleSheet(bab_getAddonInfosInstance('rssfeed')->getStylePath().'owl.transitions.css')
             .$canvas->loadScript($this->getId(), bab_getAddonInfosInstance('rssfeed')->getTemplatePath().'rssfeed.jquery.js')
             .$canvas->loadScript($this->getId(), bab_getAddonInfosInstance('rssfeed')->getTemplatePath().'owl.carousel.js')
        ;
    }
}